const express = require('express');
const conexionBD = require('./db/db');
const app = express();

//ruta raiz http://localhost:3000/
app.get('/',  async(req,res) => {
  try{
    const conexion = new conexionBD();//conectarse
    await conexion.conectarse();
    const resultados = await conexion.db.collection('tweets').find({}).toArray();//query
    conexion.desconectarse();
    //imprimir resultados en la consola
     console.log(resultados);
     //imprimir resultados en la pagina - respuesta al cliente
     res.send(resultados);
  }catch(err){//error
    console.log(err);
  }
});

app.listen(3000, () => {
  console.log('Corriendo');
});

//mongodb nombre - prueba1
